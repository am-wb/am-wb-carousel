'use strict';

describe('Create controllers', function() {

	// load the controller's module
	beforeEach(module('am-wb-carousel'));

	var controllersName = [
		'AmWbCarouselCtrl', 
	];
	var $rootScope;
	var $controller;

	// Initialize the controller and a mock scope
	beforeEach(inject(function(_$controller_, _$rootScope_) {
		$rootScope = _$rootScope_;
		$controller = _$controller_;
	}));

	it('Controllers should be defined', function() {
		for(var i=0; i<controllersName.length ; i++){		
			var scope = $rootScope.$new();
			scope.wbModel = {
					slides: []
			};
			var ctrl = $controller(controllersName[i], {$scope : scope});
			expect(angular.isDefined(ctrl)).toBe(true);
		}
	});
});