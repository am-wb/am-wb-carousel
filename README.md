# Angular Material Weburger Carousel

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/1a0dc210adae4991aeef4b422dfa93de)](https://www.codacy.com/app/am-wb/am-wb-carousel?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=am-wb/am-wb-carousel&amp;utm_campaign=Badge_Grade)
[![pipeline status](https://gitlab.com/am-wb/am-wb-carousel/badges/master/pipeline.svg)](https://gitlab.com/am-wb/am-wb-carousel/commits/master)


A widget to show one or more images as slides cycling as carousel. This is a simple carousel which does not use other libraries to implement carousel. It has dependency to angular-material-weburger.


See more details in [technical document](https://am-wb.gitlab.io/am-wb-carousel/doc/index.html).

## Development

We use these tools:

- nodejs
- grunt-cli
- bower

## Build

To build product from source you can run following commands respectively:

	nmp install
	bower install
	grunt build
	
## Rund demo

To run a demo type following commands respectively:

	npm install
	bower install
	grunt demo
	
## Use as library

This project is accessable by bower. To use this by bower run following command:

	bower install am-wb-carousel