/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * 
 * Main module of the application.
 */
angular.module('am-wb-carousel', [
	'am-wb-core'
]);

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/*
 * 
 */
angular.module('am-wb-carousel')
/*
 * 
 */
.config(function(wbIconServiceProvider) {
	wbIconServiceProvider
	// Move actions
	.addShapes({
		'wb-widget-carousel': wbIconServiceProvider.getShape('burst_mode')
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-carousel')//

/**
 * Widget controller
 * 
 * Manage an slider
 */
.controller('AmWbCarouselCtrl', function ($scope, $interval) {
    this.currentIndex = 0;
    this.slides = [];
    this.autoplayPromise = null;
    this.autoplay = false;
    this.autoplayTime = 1000;

    /**
     * Show slide with idx index
     * 
     */
    this.showSlide = function (idx) {
        if (idx === 'undefined') {
            return;
        }
        if (idx >= this.slides.length) {
            idx = 0;
        }
        if (idx < 0) {
            idx = this.slides.length - 1;
        }
        this.currentIndex = idx;
        this.slide = this.slides[this.currentIndex];
    };

    /**
     * Show next slied
     */
    this.showNext = function () {
        this.showSlide(this.currentIndex + 1);
    };

    /**
     * Skip n slied
     */
    this.plusSlides = function (n) {
        this.showSlide(this.currentIndex + n);
    };

    /**
     * Push new empty slide
     */
    this.addSlide = function () {
        this.wbModel.slides.push({
            image: 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
            caption: '<h1>Sample Caption</h1>'
        });
        this.setModelProperty('slides', this.slides);
    };

    /**
     * Remove slide in specified index
     */
    this.removeSlide = function (index) {
        this.slides.splice(index, 1);
        this.setModelProperty('slides', this.slides);
        this.showSlide(index - 1 < 0 ? 0 : index - 1);
    };


    this.manageAutoplayPromise = function () {
        var ctrl = this;
        var showNext = function () {
            ctrl.showNext();
        };

        // Remove old auto play timer
        if(this.autoplayPromise){
            $interval.cancel(this.autoplayPromise);
            this.autoplayPromise = null;
        }

        // This is not auto play widget
        if (!this.autoplay) {
            return;
        }

        var interval = Math.max(this.autoplayTime, 1000);
        this.autoplayPromise = $interval(showNext, interval);
    };

    /*
     * Init the widget
     */
    this.initWidget = function(){
        var ctrl = this;
        this.on('modelUpdated', function(event){
            var key = event.key;
            if(key === 'slides'){
                ctrl.slides = ctrl.getModelProperty('slides') || [];
                ctrl.showSlide(ctrl.currentIndex);
            }
            if(key === 'autoplayTime' || key === 'autoplay'){
                ctrl.autoplay = ctrl.getModelProperty('autoplay');
                ctrl.autoplayTime = ctrl.getModelProperty('autoplayTime') || 1000;
                ctrl.manageAutoplayPromise();
            }
        });
        // load data
        this.slides = this.getModelProperty('slides') || [];
        this.showSlide(0);
        this.autoplay = this.getModelProperty('autoplay');
        this.autoplayTime = this.getModelProperty('autoplayTime') || 1000;
        this.manageAutoplayPromise();
    }
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-carousel')

/**
 * Load settings
 */
.run(function ($settings) {
    $settings.newPage({
        type: 'carousel-setting',
        label: 'Carousel Settings',
        description: 'Set attributes of carousel',
        templateUrl: 'views/am-wb-carousel-settings/carousel.html',
        controllerAs: 'ctrl',
        /*
         * ngInject
         */
        controller: function () {
            /*
             * Watch wbModel and set values in setting when the widget is selected at first
             */
            this.init = function () {
                this.autoplay = this.getProperty('autoplay');
                this.autoplayTime = this.getProperty('autoplayTime');
                this.slides = this.getProperty('slides');
            };

            /**
             * Show slide with idx index
             * 
             */
            this.showSlide = function (idx) {
                if (idx === 'undefined') {
                    return;
                }
                if (idx >= this.slides.length) {
                    idx = 0;
                }
                if (idx < 0) {
                    idx = this.slides.length - 1;
                }
                this.setProperty('slide', this.slides[idx]);
                this.setProperty('currentIndex', idx);
            };
            /**
             * Push new empty slide
             */
            this.addSlide = function () {
                this.slides.push({
                    image: 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
                    caption: '<h1>Sample Caption</h1>'
                });
                this.setProperty('slides', this.slides);
            };

            /**
             * Remove slide in specified index
             */
            this.removeSlide = function (index) {
                this.slides.splice(index, 1);
                this.setProperty('slides', this.slides);
                this.showSlide(index - 1 < 0 ? 0 : index - 1);
            };

            this.autoplayChanged = function (autoplay) {
                this.setProperty('autoplay', autoplay);
                if (autoplay) {
                    this.autoplayTimeChanged(this.autoplayTime);
                }
            };

            this.autoplayTimeChanged = function (time) {
                this.setProperty('autoplayTime', time);
            };

            this.slidesChanged = function () {
                this.setProperty('slides', this.slides);
            };
        }
    });
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-carousel')
/**
 * load widgets
 */
.run(function($widget) {
	var slides = [{
		title : 'slide 1',
		value : 0,
		image : 'http://media.istockphoto.com/photos/starry-night-picture-id519760984',
		caption : '<h1>Caption Text for Slide 01</h1>',
		style : {}
	}, {
		title : 'slide 2',
		value : 1,
		image : 'http://media.istockphoto.com/photos/autumn-tree-and-sun-during-sunset-fall-in-park-picture-id520757917',
		caption : '<h1>Caption Text for Slide 02</h1>',
		style : {}
	}, {
		title : 'slide 3',
		value : 2,
		image : 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
		caption : '<h1>Caption Text for Slide 03</h1>',
		style : {}
	}];

	$widget.newWidget({
		// widget
		type: 'am-wb-carouselWidget',
		title : 'Carousel',
		name : 'Carousel',
		description : 'List of slides as carousel.',
		groups: ['multimedia', 'carousel'],
		icon : 'wb-widget-carousel',
		model:{
			name: 'Slider',
			slides: slides
		},
		// help
		help : 'https://gitlab.com/weburger/am-wb-carousel/wikis/home',
		helpId: '',
		// Page
		templateUrl : 'views/am-wb-carousel-widgets/simple.html',
		controller: 'AmWbCarouselCtrl',
		setting:['carousel-setting']
	});

	$widget.newWidget({
		// widget
		type: 'am-wb-carouselNavigator',
		title : 'Carousel navigator',
		name : 'Carousel navigator',
		description : 'List of slides as carousel with a navigator on top.',
		groups: ['multimedia', 'carousel'],
		icon : 'wb-widget-carousel',
		model:{
			name: 'Widget slider',
			slides: slides
		},
		// help
		help : 'https://gitlab.com/weburger/am-wb-carousel/wikis/home',
		helpId: '',
		// page
		templateUrl : 'views/am-wb-carousel-widgets/navigator.html',
		controller: 'AmWbCarouselCtrl',
		setting:['carousel-setting']
	});
});

angular.module('am-wb-carousel').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-carousel-settings/carousel.html',
    "<fieldset layout=column> <legend translate>Settings</legend> <wb-ui-setting-on-off-switch title=Autoplay? icon=play_circle_outline ng-model=ctrl.autoplay ng-change=ctrl.autoplayChanged(ctrl.autoplay)> </wb-ui-setting-on-off-switch> <wb-ui-setting-number ng-init=\"ctrl.autoplayTime = ctrl.autoplayTime || 1000\" ng-show=ctrl.autoplay title=\"Time (millisecond)\" icon=schedule ng-model=ctrl.autoplayTime ng-change=ctrl.autoplayTimeChanged(ctrl.autoplayTime)> </wb-ui-setting-number> </fieldset> <div layout=row> <md-button flex=10 class=md-icon-button ng-click=ctrl.addSlide() aria-label=\"Add new slide\"> <wb-icon>add</wb-icon> </md-button> </div>  <fieldset ng-repeat=\"slide in ctrl.slides\" layout=column> <legend><span translate>Slide</span>#{{$index +1}}</legend> <wb-ui-setting-image ng-model=slide.image ng-change=ctrl.slidesChanged() title=Image aria-label=Image> </wb-ui-setting-image> <md-input-container class=\"md-icon-float md-block\"> <textarea ng-model=slide.caption ng-change=ctrl.slidesChanged() ng-model-options=\"{debounce: 200}\" type=text aria-label=Caption></textarea> </md-input-container> <div layout=row> <span flex></span> <md-button flex=10 class=md-icon-button ng-click=ctrl.removeSlide($index) aria-label=\"Remove slide\"> <wb-icon>delete</wb-icon> </md-button> </div> </fieldset>"
  );


  $templateCache.put('views/am-wb-carousel-widgets/navigator.html',
    "<div class=carousel-navigator-slideshow-container> <div layout=row ng-style=\"{'padding-left' : editable ? '30px' : '10px'}\" style=padding-right:16px> <img class=carousel-navigator-thumnail data-ng-repeat=\"slide in wbModel.slides\" data-ng-src={{slide.image}} data-ng-click=ctrl.showSlide($index)> <md-button ng-show=ctrl.editable class=carousel-navigator-thumnail ng-click=ctrl.addSlide()> <wb-icon>add</wb-icon> </md-button> <span flex></span> <div layout=row flex=10 layout-align=\"end center\"> <md-button ng-show=ctrl.editable class=md-icon-button ng-click=ctrl.removeSlide(wbModel.currentIndex)> <wb-icon>delete</wb-icon> </md-button> </div> </div> <div class=carousel-navigator-fade> <img class=carousel-navigator-img src={{ctrl.slide.image}} alt={{ctrl.slide.image}}> <div class=carousel-navigator-text ng-hide=ctrl.isSelected() ng-bind-html=\"ctrl.slide.caption | wbunsafe\"> </div> <a class=carousel-simple-prev data-ng-click=ctrl.plusSlides(-1)>&#10094;</a> <a class=carousel-simple-next data-ng-click=ctrl.plusSlides(1)>&#10095;</a> </div> </div>"
  );


  $templateCache.put('views/am-wb-carousel-widgets/simple.html',
    "<div class=carousel-simple-slideshow-container> <div class=carousel-simple-fade> <img class=carousel-simple-img src={{ctrl.slide.image}} alt={{ctrl.slide.image}}> <div class=carousel-simple-text ng-hide=ctrl.isSelected() ng-bind-html=\"ctrl.slide.caption | wbunsafe\"> </div> <div class=carousel-simple-dots> <span data-ng-repeat=\"slide in ctrl.slides\" data-ng-class=\"{'carousel-simple-dot': true, 'carousel-simple-active': wbModel.currentIndex === $index }\" data-ng-click=ctrl.showSlide($index)> </span> </div> </div> <a class=carousel-simple-prev data-ng-click=ctrl.plusSlides(-1)>&#10094;</a> <a class=carousel-simple-next data-ng-click=ctrl.plusSlides(1)>&#10095;</a> </div>"
  );

}]);
