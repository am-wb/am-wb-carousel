/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

//Test controller
angular.module('MyTestModule', ['am-wb-carousel', 'jsonFormatter'])
	.controller('MyTestCtrl', function ($scope, $http, $wbUtil, $wbFloat, $widget, WidgetLocatorManager) {
	    $http.get('temp.json')//
		    .then(function (res) {
			// NOTE: maso, 2018: clean data model
			$scope.model = $wbUtil.clean(res.data);
		    });
	    // load setting of model
	    var ctrl = this;
	    /*
	     * Display an area of a widget with extra informations. It must be set to
	     * visible.
	     */
	    var widgetLocator = new WidgetLocatorManager({
		anchor: '#demo-anchor'
	    });
	    this.widgetLocator = widgetLocator;


	    $http.get('examples/html.json')
		    .then(function (res) {
			// NOTE: maso, 2018: clean data model
			$scope.model = $wbUtil.clean(res.data);
		    });

	    // load setting of model
	    $scope.loadSettings = function ($event) {
		widgetLocator.setSelectedWidgets($event.widgets || []);
		// TODO: maso, 2018: support list of widget in settings too
		var widgets = $event.widgets;
		if (widgets.length) {
		    var widget = widgets[0];
		    $scope.selectedWidget = widget;
		    $scope.actions = widget.getActions();
		} else {
		    $scope.selectedWidget = null;
		    $scope.actions = null;
		}
	    };


	    $scope.runAction = function (action) {
		action.action();
	    };

	    // load widgets
	    $widget.widgets().then(function (list) {
		$scope.widgets = list.items;
	    });

	    var headerControls = {
		close: 'remove',
		maximize: 'remove'
	    };

	    function openWidgets() {
		var watch;
		$wbFloat.show({
		    title: 'Widgets',
		    template: '<wb-widgets-explorer ng-model="widgets"></wb-widgets-explorer>',
		    parent: $scope,
		    controller: function ($wbFloat) {
			watch = $scope.$watch('editable', function (value) {
			    if (value === false) {
				$wbFloat.hide();
			    }
			});
		    },
		    // Extera options
		    headerControls: headerControls,
		    panelSize: '400 600',
		    position: {
			my: 'right-top',
			at: 'right-top',
			autoposition: 'down',
			offsetX: -5,
			offsetY: 5
		    }
		})
			.finally(function () {
			    watch();
			});
	    }

	    function openSettings() {
		var watch, watch2;
		var parent = $scope;
		$wbFloat.show({
		    title: 'Settings',
		    template: '<wb-setting-panel-group wb-tab-mode ng-model="model"></wb-setting-panel-group>',
		    parent: $scope,
		    controller: function ($scope, $wbFloat) {
			watch = parent.$watch('editable', function (value) {
			    if (value === false) {
				$wbFloat.hide();
			    }
			});
			watch2 = parent.$watch('selectedWidget', function (value) {
			    $scope.model = value;
			});
		    },
		    // Extera options
		    headerControls: headerControls,
		    position: {
			my: 'left-top',
			at: 'left-top',
			autoposition: 'down',
			offsetX: -5,
			offsetY: 5
		    }
		})
			.finally(function () {
			    watch();
			    watch2();
			});
	    }

	    function openContent() {
		var watch, watch2;
		var parent = $scope;
		$wbFloat.show({
		    title: 'Content',
		    template: '<json-formatter json="model" open="1"></json-formatter>',
		    parent: parent,
		    controller: function ($scope, $wbFloat) {
			watch = parent.$watch('editable', function (value) {
			    if (value === false) {
				$wbFloat.hide();
			    }
			});
			watch2 = parent.$watch('selectedWidget', function (value) {
			    $scope.model = value;
			});
		    },
		    // Extera options
		    headerControls: headerControls,
		    position: {
			my: 'left-top',
			at: 'left-top',
			autoposition: 'down',
			offsetX: -5,
			offsetY: 5
		    }
		})
			.finally(function () {
			    watch();
			    watch2();
			});
	    }

	    $scope.$watch('editable', function (value) {
		if (value) {
		    openWidgets();
		    openSettings();
		    openContent();
		    if (!widgetLocator.isEnable()) {
			widgetLocator.setEnable(true);
		    }
		}
		widgetLocator.setVisible(value);
	    });
	});