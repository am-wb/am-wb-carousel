/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-carousel')

/**
 * Load settings
 */
.run(function ($settings) {
    $settings.newPage({
        type: 'carousel-setting',
        label: 'Carousel Settings',
        description: 'Set attributes of carousel',
        templateUrl: 'views/am-wb-carousel-settings/carousel.html',
        controllerAs: 'ctrl',
        /*
         * ngInject
         */
        controller: function () {
            /*
             * Watch wbModel and set values in setting when the widget is selected at first
             */
            this.init = function () {
                this.autoplay = this.getProperty('autoplay');
                this.autoplayTime = this.getProperty('autoplayTime');
                this.slides = this.getProperty('slides');
            };

            /**
             * Show slide with idx index
             * 
             */
            this.showSlide = function (idx) {
                if (idx === 'undefined') {
                    return;
                }
                if (idx >= this.slides.length) {
                    idx = 0;
                }
                if (idx < 0) {
                    idx = this.slides.length - 1;
                }
                this.setProperty('slide', this.slides[idx]);
                this.setProperty('currentIndex', idx);
            };
            /**
             * Push new empty slide
             */
            this.addSlide = function () {
                this.slides.push({
                    image: 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
                    caption: '<h1>Sample Caption</h1>'
                });
                this.setProperty('slides', this.slides);
            };

            /**
             * Remove slide in specified index
             */
            this.removeSlide = function (index) {
                this.slides.splice(index, 1);
                this.setProperty('slides', this.slides);
                this.showSlide(index - 1 < 0 ? 0 : index - 1);
            };

            this.autoplayChanged = function (autoplay) {
                this.setProperty('autoplay', autoplay);
                if (autoplay) {
                    this.autoplayTimeChanged(this.autoplayTime);
                }
            };

            this.autoplayTimeChanged = function (time) {
                this.setProperty('autoplayTime', time);
            };

            this.slidesChanged = function () {
                this.setProperty('slides', this.slides);
            };
        }
    });
});
