/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-carousel')//

/**
 * Widget controller
 * 
 * Manage an slider
 */
.controller('AmWbCarouselCtrl', function ($scope, $interval) {
    this.currentIndex = 0;
    this.slides = [];
    this.autoplayPromise = null;
    this.autoplay = false;
    this.autoplayTime = 1000;

    /**
     * Show slide with idx index
     * 
     */
    this.showSlide = function (idx) {
        if (idx === 'undefined') {
            return;
        }
        if (idx >= this.slides.length) {
            idx = 0;
        }
        if (idx < 0) {
            idx = this.slides.length - 1;
        }
        this.currentIndex = idx;
        this.slide = this.slides[this.currentIndex];
    };

    /**
     * Show next slied
     */
    this.showNext = function () {
        this.showSlide(this.currentIndex + 1);
    };

    /**
     * Skip n slied
     */
    this.plusSlides = function (n) {
        this.showSlide(this.currentIndex + n);
    };

    /**
     * Push new empty slide
     */
    this.addSlide = function () {
        this.wbModel.slides.push({
            image: 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
            caption: '<h1>Sample Caption</h1>'
        });
        this.setModelProperty('slides', this.slides);
    };

    /**
     * Remove slide in specified index
     */
    this.removeSlide = function (index) {
        this.slides.splice(index, 1);
        this.setModelProperty('slides', this.slides);
        this.showSlide(index - 1 < 0 ? 0 : index - 1);
    };


    this.manageAutoplayPromise = function () {
        var ctrl = this;
        var showNext = function () {
            ctrl.showNext();
        };

        // Remove old auto play timer
        if(this.autoplayPromise){
            $interval.cancel(this.autoplayPromise);
            this.autoplayPromise = null;
        }

        // This is not auto play widget
        if (!this.autoplay) {
            return;
        }

        var interval = Math.max(this.autoplayTime, 1000);
        this.autoplayPromise = $interval(showNext, interval);
    };

    /*
     * Init the widget
     */
    this.initWidget = function(){
        var ctrl = this;
        this.on('modelUpdated', function(event){
            var key = event.key;
            if(key === 'slides'){
                ctrl.slides = ctrl.getModelProperty('slides') || [];
                ctrl.showSlide(ctrl.currentIndex);
            }
            if(key === 'autoplayTime' || key === 'autoplay'){
                ctrl.autoplay = ctrl.getModelProperty('autoplay');
                ctrl.autoplayTime = ctrl.getModelProperty('autoplayTime') || 1000;
                ctrl.manageAutoplayPromise();
            }
        });
        // load data
        this.slides = this.getModelProperty('slides') || [];
        this.showSlide(0);
        this.autoplay = this.getModelProperty('autoplay');
        this.autoplayTime = this.getModelProperty('autoplayTime') || 1000;
        this.manageAutoplayPromise();
    }
});
